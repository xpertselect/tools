<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\Tools;

/**
 * Class GitIgnoreGenerator.
 *
 * Service responsible for generating the `.gitignore` files at the appropriate locations.
 */
final readonly class GitIgnoreGenerator
{
    /**
     * GitIgnoreGenerator constructor.
     *
     * @param ProjectType $projectType  The type of project to reason about
     * @param string      $projectRoot  The root of the project to reason about
     * @param mixed       $outputStream The stream to write to
     */
    public function __construct(private ProjectType $projectType,
                                private string $projectRoot,
                                private mixed $outputStream = STDERR)
    {
    }

    /**
     * Generate all the `.gitignore` files for the current project.
     */
    public function generateGitIgnoreFiles(): void
    {
        $this->generateToolsIgnoreFile();
        $this->generateDrupalIgnoreFiles();
    }

    /**
     * Generate the `.gitignore` file for the `resources/tools` directory.
     */
    private function generateToolsIgnoreFile(): void
    {
        $path = $this->projectRoot . '/resources/tools';

        $this->ensureDirectoryExists($path);
        $this->placeIgnoreFile($path, true);
    }

    /**
     * Generate the `.gitignore` files required for a Drupal project.
     */
    private function generateDrupalIgnoreFiles(): void
    {
        if ('drupal-project' !== $this->projectType->value) {
            return;
        }

        $customPaths = [
            $this->projectRoot . '/web/modules/custom',
            $this->projectRoot . '/web/profiles/custom',
            $this->projectRoot . '/web/themes/custom',
        ];
        $contribPaths = [
            $this->projectRoot . '/web/modules/contrib',
            $this->projectRoot . '/web/profiles/contrib',
            $this->projectRoot . '/web/themes/contrib',
        ];

        foreach ($customPaths as $path) {
            fwrite($this->outputStream, "Generating .gitignore file for {$path}" . PHP_EOL);

            $this->ensureDirectoryExists($path);
            $this->placeIgnoreFile($path, false);
        }

        foreach ($contribPaths as $path) {
            fwrite($this->outputStream, "Generating .gitignore file for {$path}" . PHP_EOL);

            $this->ensureDirectoryExists($path);
            $this->placeIgnoreFile($path, true);
        }
    }

    /**
     * Ensure a directory exists by creating it if it does not.
     *
     * @param string $path The directory path to check
     */
    private function ensureDirectoryExists(string $path): void
    {
        if (!is_readable($path)) {
            mkdir($path, recursive: true);
        }
    }

    /**
     * Generate a `.gitignore` file in the given directory.
     *
     * @param string $path           The directory to receive a `.gitignore` file
     * @param bool   $ignoreContents Whether the `.gitignore` file should ignore all contents of the
     *                               directory
     */
    private function placeIgnoreFile(string $path, bool $ignoreContents): void
    {
        $filePath = $path . '/.gitignore';

        if ($ignoreContents) {
            file_put_contents($filePath, "*\n!.gitignore\n");
        } else {
            touch($filePath);
        }
    }
}
