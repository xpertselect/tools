<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\Tools;

use RuntimeException;

final readonly class App
{
    /**
     * Initializes the App instance based on the given project root and CLI arguments available to
     * the PHP process.
     *
     * Uses `getopt()` internally to determine how to configure the instance.
     *
     * @param string $path The root of the Composer project
     *
     * @return App The App instance
     */
    public static function initialize(string $path): App
    {
        $cliInput = getopt('', ['type:', 'force:']);

        if (empty($cliInput['type'])) {
            throw new RuntimeException('--type argument is missing.');
        }

        if (empty($cliInput['force'])) {
            $cliInput['force'] = false;
        }

        if (is_array($cliInput['type'])) {
            throw new RuntimeException('--force argument must be a string value.');
        }

        $type = ProjectType::tryFrom(strval($cliInput['type']));

        if (null === $type) {
            throw new RuntimeException('Unknown type provided.');
        }

        return new App(
            new ConfigurationFilePublisher($type, $path, 'true' === $cliInput['force']),
            new GitIgnoreGenerator($type, $path)
        );
    }

    /**
     * App constructor.
     *
     * @param ConfigurationFilePublisher $publisher The path of the Composer project
     * @param GitIgnoreGenerator         $generator The type of project
     */
    public function __construct(private ConfigurationFilePublisher $publisher,
                                private GitIgnoreGenerator $generator)
    {
    }

    /**
     * Publishes all the configuration files.
     *
     * @return $this This instance, for method chaining
     */
    public function publishFiles(): self
    {
        $this->publisher->publishFiles();

        return $this;
    }

    /**
     * Generates the appropriate .gitignore files in the various subdirectories.
     *
     * @return $this This instance, for method chaining
     */
    public function generateGitIgnoreFiles(): self
    {
        $this->generator->generateGitIgnoreFiles();

        return $this;
    }
}
