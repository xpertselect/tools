<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\Tools;

/**
 * StringBackedEnum ProjectType.
 *
 * An Enum that represents the type of project to reason about.
 *
 * - **`Standard`** <br>
 *   Represents a standard PHP project.
 *
 * - **`DrupalProject`** <br>
 *   Represents a Drupal project.
 *
 * - **`DrupalPackage`** <br>
 *   Represents a Drupal package.
 *
 * - **`LaravelProject`** <br>
 *   Represents a Laravel project.
 *
 * - **`LaravelPackage`** <br>
 *   Represents a Laravel package.
 */
enum ProjectType: string
{
    /**
     * Retrieve the path to the configuration directory.
     *
     * @return string The path
     */
    public function directory(): string
    {
        return __DIR__ . '/../resources/config/' . $this->value;
    }

    /**
     * Retrieve the configuration files to publish for this projectType.
     *
     * @return string[] The configuration files
     */
    public function configurationFiles(): array
    {
        return [
            'phive.xml',
            'phpstan.neon.dist',
            'phpunit.xml.dist',
            '.php-cs-fixer.dist.php',
            '.yamllint.yml',
        ];
    }

    /**
     * The PHP-CS-Fixer ruleset file to use for this projectType.
     *
     * @return string The path to the ruleset file
     */
    public function phpCsFixerRuleFile(): string
    {
        $baseDirectory = __DIR__ . '/../resources/rules/';

        return match ($this) {
            self::Standard                             => $baseDirectory . 'standard.rules.php',
            self::DrupalProject, self::DrupalPackage   => $baseDirectory . 'drupal.rules.php',
            self::LaravelProject, self::LaravelPackage => $baseDirectory . 'laravel.rules.php',
        };
    }

    case Standard       = 'standard';
    case DrupalProject  = 'drupal-project';
    case DrupalPackage  = 'drupal-package';
    case LaravelProject = 'laravel-project';
    case LaravelPackage = 'laravel-package';
}
