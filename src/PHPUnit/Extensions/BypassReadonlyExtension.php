<?php

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\Tools\PHPUnit\Extensions;

use PHPUnit\Runner\Extension\Extension;
use PHPUnit\Runner\Extension\Facade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;
use Zfekete\BypassReadonly\BypassReadonly;

/**
 * Class BypassReadonlyExtension.
 *
 * PHPUnit extension to disable `readonly` and `final` modifiers in the PHP codebase to ensure all
 * classes can be mocked by libraries such as Mockery.
 *
 * @internal
 */
final class BypassReadonlyExtension implements Extension
{
    /**
     * Disable any `readonly` and `final` modifiers prior to executing the test to ensure that
     * readonly and final classes can properly be mocked during tests.
     */
    public function bootstrap(Configuration $configuration, Facade $facade, ParameterCollection $parameters): void
    {
        BypassReadonly::enable();
    }
}
