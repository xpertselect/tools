<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\Tools;

/**
 * Class ConfigurationFilePublisher.
 *
 * Service for publishing the configuration files that should belong to a given project.
 */
final readonly class ConfigurationFilePublisher
{
    /**
     * ConfigurationFilePublisher constructor.
     *
     * @param ProjectType $projectType  The type of project to reason about
     * @param string      $projectRoot  The root of the project
     * @param bool        $replace      Whether to replace previous configuration files
     * @param mixed       $outputStream The stream to write to
     */
    public function __construct(private ProjectType $projectType,
                                private string $projectRoot,
                                private bool $replace = false,
                                private mixed $outputStream = STDERR)
    {
    }

    /**
     * Publish all the configuration files offered for the projectType.
     */
    public function publishFiles(): void
    {
        foreach ($this->projectType->configurationFiles() as $file) {
            $this->publishFile($file);
        }
    }

    /**
     * Publish a configuration file into the root of the project.
     *
     * @param string $file The file to publish
     */
    public function publishFile(string $file): void
    {
        $filePath = $this->projectRoot . '/' . $file;

        if (is_file($filePath) && !$this->replace) {
            fwrite($this->outputStream, "File {$file} already exists" . PHP_EOL);

            return;
        }

        $source = $this->projectType->directory() . '/' . $file;
        $result = copy($source, $filePath);

        if ($result) {
            fwrite($this->outputStream, "Published {$file} configuration file" . PHP_EOL);
        } else {
            fwrite($this->outputStream, "Failed to publish {$file} configuration file" . PHP_EOL);
        }
    }
}
