<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\PHPUnit\Extensions;

use PHPUnit\Runner\Extension\Extension;
use Tests\TestCase;
use XpertSelect\Tools\PHPUnit\Extensions\BypassReadonlyExtension;

/**
 * @internal
 */
final class BypassReadonlyExtensionTest extends TestCase
{
    public function testExtensionImplementsCorrectInterface(): void
    {
        $this->assertInstanceOf(Extension::class, new BypassReadonlyExtension());
    }
}
