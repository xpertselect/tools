<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Tests\TestCase;
use XpertSelect\Tools\App;
use XpertSelect\Tools\ConfigurationFilePublisher;
use XpertSelect\Tools\GitIgnoreGenerator;

/**
 * @internal
 */
final class AppTest extends TestCase
{
    public function testAppDelegatesRequestsToItsDependencies(): void
    {
        $mockedPublisher = $this->getMockBuilder(ConfigurationFilePublisher::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['publishFiles'])
            ->getMock();
        $mockedPublisher->expects($this->once())
            ->method('publishFiles');

        $mockedGenerator = $this->getMockBuilder(GitIgnoreGenerator::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['generateGitIgnoreFiles'])
            ->getMock();
        $mockedGenerator->expects($this->once())
            ->method('generateGitIgnoreFiles');

        $app = new App($mockedPublisher, $mockedGenerator);
        $app->publishFiles();
        $app->generateGitIgnoreFiles();

        $this->assertTrue(true);
    }
}
