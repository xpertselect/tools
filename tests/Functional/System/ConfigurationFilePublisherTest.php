<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\System;

use Tests\ProjectTypesDataset;
use Tests\TestCase;
use XpertSelect\Tools\ConfigurationFilePublisher;
use XpertSelect\Tools\ProjectType;

/**
 * @internal
 */
final class ConfigurationFilePublisherTest extends TestCase
{
    use ProjectTypesDataset;

    /**
     * @dataProvider projectTypes
     */
    public function testConfigurationFilesArePublished(ProjectType $projectType): void
    {
        $workingDirectory = $this->createTemporaryDirectory();

        foreach ($projectType->configurationFiles() as $file) {
            $this->assertFileDoesNotExist($workingDirectory . '/' . $file);
        }

        $publisher = new ConfigurationFilePublisher(
            $projectType,
            $workingDirectory,
            false,
            fopen('php://memory', 'rw')
        );

        $publisher->publishFiles();

        foreach ($projectType->configurationFiles() as $file) {
            $this->assertFileExists($workingDirectory . '/' . $file);
        }
    }
}
