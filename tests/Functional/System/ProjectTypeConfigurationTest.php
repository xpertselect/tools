<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\System;

use Tests\ProjectTypesDataset;
use Tests\TestCase;
use XpertSelect\Tools\ProjectType;

/**
 * @internal
 */
final class ProjectTypeConfigurationTest extends TestCase
{
    use ProjectTypesDataset;

    /**
     * @dataProvider projectTypes
     */
    public function testProjectTypesConfigurationDirectoryExists(ProjectType $projectType): void
    {
        $this->assertDirectoryExists($projectType->directory());
    }

    /**
     * @dataProvider projectTypes
     */
    public function testProjectTypesConfigurationFilesExist(ProjectType $projectType): void
    {
        foreach ($projectType->configurationFiles() as $file) {
            $this->assertFileExists($projectType->directory() . '/' . $file);
        }
    }

    /**
     * @dataProvider projectTypes
     */
    public function testReferencedRuleFileExists(ProjectType $projectType): void
    {
        $this->assertFileExists($projectType->phpCsFixerRuleFile());
    }

    /**
     * @dataProvider projectTypes
     */
    public function testRuleFileReturnsAnArray(ProjectType $projectType): void
    {
        $rules = include $projectType->phpCsFixerRuleFile();

        $this->assertIsArray($rules);
    }
}
