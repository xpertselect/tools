<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\System;

use Tests\TestCase;
use XpertSelect\Tools\GitIgnoreGenerator;
use XpertSelect\Tools\ProjectType;

/**
 * @internal
 */
final class GitIgnoreGeneratorTest extends TestCase
{
    public function testToolsIgnoreFileIsGeneratedForStandardProject(): void
    {
        $workingDirectory = $this->createTemporaryDirectory();

        $this->assertFileDoesNotExist($workingDirectory . '/resources/tools/.gitignore');
        $this->assertDirectoryDoesNotExist($workingDirectory . '/web');

        $generator = new GitIgnoreGenerator(
            ProjectType::Standard,
            $workingDirectory,
            fopen('php://memory', 'rw')
        );

        $generator->generateGitIgnoreFiles();

        $this->assertFileExists($workingDirectory . '/resources/tools/.gitignore');
        $this->assertDirectoryDoesNotExist($workingDirectory . '/web');
    }

    public function testIgnoreFilesAreGeneratedForDrupalProject(): void
    {
        $workingDirectory = $this->createTemporaryDirectory();

        $this->assertFileDoesNotExist($workingDirectory . '/resources/tools/.gitignore');
        $this->assertDirectoryDoesNotExist($workingDirectory . '/web');

        $generator = new GitIgnoreGenerator(
            ProjectType::DrupalProject,
            $workingDirectory,
            fopen('php://memory', 'rw')
        );

        $generator->generateGitIgnoreFiles();

        $this->assertFileExists($workingDirectory . '/resources/tools/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/modules/contrib/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/modules/custom/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/profiles/contrib/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/profiles/custom/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/themes/contrib/.gitignore');
        $this->assertFileExists($workingDirectory . '/web/themes/custom/.gitignore');
    }
}
