<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional\CLI;

use Tests\TestCase;

/**
 * @internal
 */
class XsConfigTest extends TestCase
{
    public function testPlaceholder(): void
    {
        $this->assertTrue(true);
    }
}
