<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests;

use XpertSelect\Tools\ProjectType;

/**
 * @internal
 */
trait ProjectTypesDataset
{
    public static function projectTypes(): array
    {
        $projectTypes = [];

        foreach (ProjectType::cases() as $type) {
            $projectTypes[$type->name] = [$type];
        }

        return $projectTypes;
    }
}
