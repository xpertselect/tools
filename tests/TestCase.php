<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
    protected function createTemporaryDirectory(): string
    {
        $temporaryFile = tempnam(sys_get_temp_dir(), 'phpunit-');

        if (file_exists($temporaryFile)) {
            unlink($temporaryFile);
        }

        mkdir($temporaryFile);

        return $temporaryFile;
    }
}
