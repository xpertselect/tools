<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

$rules = include __DIR__ . '/standard.rules.php';

$rules['constant_case']['case'] = 'upper';

return $rules;
