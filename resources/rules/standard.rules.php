<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

$aliasRules = [
    'no_alias_language_construct_call' => true,
    'no_mixed_echo_print'              => [
        'use' => 'echo',
    ],
];

$arrayRules = [
    'array_syntax' => [
        'syntax' => 'short',
    ],
    'no_multiline_whitespace_around_double_arrow' => true,
    'no_whitespace_before_comma_in_array'         => [
        'after_heredoc' => true,
    ],
    'normalize_index_brace'           => true,
    'trim_array_spaces'               => true,
    'whitespace_after_comma_in_array' => [
        'ensure_single_space' => false,
    ],
];

$attributeNotationRules = [
    'attribute_empty_parentheses' => [
        'use_parentheses' => false,
    ],
];

$basicRules = [
    'braces_position' => [
        'allow_single_line_anonymous_functions'     => true,
        'allow_single_line_empty_anonymous_classes' => true,
        'anonymous_classes_opening_brace'           => 'same_line',
        'anonymous_functions_opening_brace'         => 'same_line',
        'classes_opening_brace'                     => 'next_line_unless_newline_at_signature_end',
        'control_structures_opening_brace'          => 'same_line',
        'functions_opening_brace'                   => 'next_line_unless_newline_at_signature_end',
    ],
    'encoding'                                    => true,
    'no_multiple_statements_per_line'             => true,
    'no_trailing_comma_in_singleline'             => true,
];

$casingRules = [
    'constant_case' => [
        'case' => 'lower',
    ],
    'integer_literal_case'                    => true,
    'lowercase_keywords'                      => true,
    'lowercase_static_reference'              => true,
    'magic_constant_casing'                   => true,
    'magic_method_casing'                     => true,
    'native_function_casing'                  => true,
    'native_type_declaration_casing'          => true,
];

$castNotationRules = [
    'cast_spaces' => [
        'space' => 'single',
    ],
    'lowercase_cast'     => true,
    'no_short_bool_cast' => true,
    'no_unset_cast'      => true,
    'short_scalar_cast'  => true,
];

$classNotationRules = [
    'class_attributes_separation' => [
        'elements' => [
            'const'        => 'one',
            'method'       => 'one',
            'property'     => 'one',
            'trait_import' => 'none',
            'case'         => 'none',
        ],
    ],
    'class_definition' => [
        'multi_line_extends_each_single_line' => true,
        'single_item_single_line'             => true,
        'single_line'                         => true,
        'space_before_parenthesis'            => true,
    ],
    'no_blank_lines_after_class_opening' => true,
    'no_null_property_initialization'    => true,
    'ordered_class_elements'             => [
        'order' => [
            'use_trait',
            'constant_public',
            'constant_protected',
            'constant_private',
            'property_public_static',
            'property_protected_static',
            'property_private_static',
            'property_public_readonly',
            'property_public',
            'property_protected_readonly',
            'property_protected',
            'property_private_readonly',
            'property_private',
            'method_public_abstract_static',
            'method_public_static',
            'method_protected_abstract_static',
            'method_protected_static',
            'method_private_static',
            'construct',
            'destruct',
            'magic',
            'phpunit',
            'method_public_abstract',
            'method_public',
            'method_protected_abstract',
            'method_protected',
            'method_private',
        ],
        'sort_algorithm' => 'none',
    ],
    'protected_to_private'               => true,
    'self_static_accessor'               => true,
    'single_class_element_per_statement' => [
        'elements' => [
            'const',
            'property',
        ],
    ],
    'single_trait_insert_per_statement' => true,
    'visibility_required'               => [
        'elements' => [
            'property',
            'method',
            'const',
        ],
    ],
];

$classUsageRules = [
];

$commentRules = [
    'header_comment' => [
        'comment_type' => 'PHPDoc',
        'header'       => null,
        'location'     => 'after_declare_strict',
        'separate'     => 'both',
    ],
    'multiline_comment_opening_closing' => false,
    'no_empty_comment'                  => true,
    'no_trailing_whitespace_in_comment' => true,
    'single_line_comment_style'         => [
        'comment_types' => [
            'asterisk',
            'hash',
        ],
    ],
];

$constantNotationRules = [
];

$controlStructureRules = [
    'control_structure_braces'                => true,
    'control_structure_continuation_position' => [
        'position' => 'same_line',
    ],
    'elseif'          => true,
    'empty_loop_body' => [
        'style' => 'braces',
    ],
    'empty_loop_condition' => [
        'style' => 'while',
    ],
    'include'               => true,
    'no_alternative_syntax' => [
        'fix_non_monolithic_code' => true,
    ],
    'no_break_comment' => [
        'comment_text' => 'No break.',
    ],
    'no_superfluous_elseif'            => true,
    'no_unneeded_braces'               => [
        'namespaces' => true,
    ],
    'no_unneeded_control_parentheses'  => [
        'statements' => [
            'break',
            'clone',
            'continue',
            'echo_print',
            'return',
            'switch_case',
            'yield',
            'yield_from',
        ],
    ],
    'no_useless_else'                => true,
    'simplified_if_return'           => true,
    'switch_case_semicolon_to_colon' => true,
    'switch_case_space'              => true,
    'switch_continue_to_break'       => true,
    'trailing_comma_in_multiline'    => [
        'after_heredoc' => true,
        'elements'      => [
            'arrays',
        ],
    ],
    'yoda_style' => [
        'always_move_variable' => true,
        'equal'                => true,
        'identical'            => true,
        'less_and_greater'     => null,
    ],
];

$doctrineAnnotationRules = [
];

$functionNotationRules = [
    'function_declaration' => [
        'closure_function_spacing' => 'none',
    ],
    'lambda_not_used_import'  => true,
    'method_argument_space'   => [
        'after_heredoc'                    => true,
        'keep_multiple_spaces_after_comma' => false,
        'on_multiline'                     => 'ignore',
    ],
    'no_spaces_after_function_name'                    => true,
    'nullable_type_declaration_for_default_null_value' => [
        'use_nullable_type_declaration' => true,
    ],
    'return_type_declaration' => [
        'space_before' => 'none',
    ],
    'single_line_throw' => false,
];

$importRules = [
    'fully_qualified_strict_types' => [
        'import_symbols'                        => true,
        'leading_backslash_in_global_namespace' => false,
        'phpdoc_tags'                           => [
            'param',
            'phpstan-param',
            'phpstan-property',
            'phpstan-property-read',
            'phpstan-property-write',
            'phpstan-return',
            'phpstan-var',
            'property',
            'property-read',
            'property-write',
            'psalm-param',
            'psalm-property',
            'psalm-property-read',
            'psalm-property-write',
            'psalm-return',
            'psalm-var',
            'return',
            'see',
            'throws',
            'var',
        ],
    ],
    'global_namespace_import'      => [
        'import_classes'   => true,
        'import_constants' => true,
        'import_functions' => true,
    ],
    'group_import'            => false,
    'no_leading_import_slash' => true,
    'no_unused_imports'       => true,
    'ordered_imports'         => [
        'imports_order'  => [
            'const',
            'function',
            'class',
        ],
        'sort_algorithm' => 'alpha',
    ],
    'single_import_per_statement' => [
        'group_to_single_imports' => true,
    ],
    'single_line_after_imports'   => true,
];

$languageConstructRules = [
    'combine_consecutive_issets' => true,
    'combine_consecutive_unsets' => true,
    'declare_equal_normalize'    => [
        'space' => 'none',
    ],
    'declare_parentheses'           => true,
    'explicit_indirect_variable'    => true,
    'single_space_around_construct' => [
        'constructs_followed_by_a_single_space' => [
            'abstract',
            'as',
            'attribute',
            'break',
            'case',
            'catch',
            'class',
            'clone',
            'comment',
            'const',
            'const_import',
            'continue',
            'do',
            'echo',
            'else',
            'elseif',
            'enum',
            'extends',
            'final',
            'finally',
            'for',
            'foreach',
            'function',
            'function_import',
            'global',
            'goto',
            'if',
            'implements',
            'include',
            'include_once',
            'instanceof',
            'insteadof',
            'interface',
            'match',
            'named_argument',
            'namespace',
            'new',
            'open_tag_with_echo',
            'php_doc',
            'php_open',
            'print',
            'private',
            'protected',
            'public',
            'readonly',
            'require',
            'require_once',
            'return',
            'static',
            'switch',
            'throw',
            'trait',
            'try',
            'use',
            'use_lambda',
            'use_trait',
            'var',
            'while',
            'yield',
            'yield_from',
        ],
    ],
];

$listNotationRules = [
    'list_syntax' => [
        'syntax' => 'short',
    ],
];

$namespaceNotationRules = [
    'blank_line_after_namespace'         => true,
    'blank_lines_before_namespace'       => [
        'max_line_breaks' => 2,
        'min_line_breaks' => 2,
    ],
    'clean_namespace'                    => true,
    'no_leading_namespace_whitespace'    => true,
];

$namingRules = [
];

$operatorRules = [
    'assign_null_coalescing_to_coalesce_equal' => false,
    'binary_operator_spaces'                   => [
        'default'   => 'align_single_space',
        'operators' => [],
    ],
    'concat_space' => [
        'spacing' => 'one',
    ],
    'increment_style' => [
        'style' => 'pre',
    ],
    'new_with_parentheses'                    => [
        'anonymous_class' => true,
        'named_class'     => true,
    ],
    'no_space_around_double_colon'            => true,
    'not_operator_with_space'                 => false,
    'not_operator_with_successor_space'       => false,
    'object_operator_without_whitespace'      => true,
    'operator_linebreak'                      => [
        'only_booleans' => true,
        'position'      => 'beginning',
    ],
    'standardize_increment'      => true,
    'standardize_not_equals'     => true,
    'ternary_operator_spaces'    => true,
    'ternary_to_null_coalescing' => true,
    'unary_operator_spaces'      => [
        'only_dec_inc' => false,
    ],
];

$phpTagRules = [
    'blank_line_after_opening_tag' => true,
    'echo_tag_syntax'              => [
        'format'                         => 'long',
        'long_function'                  => 'echo',
        'shorten_simple_statements_only' => false,
    ],
    'full_opening_tag'            => true,
    'linebreak_after_opening_tag' => true,
    'no_closing_tag'              => true,
];

$phpUnitRules = [
    'php_unit_fqcn_annotation' => true,
    'php_unit_internal_class'  => [
        'types' => [
            'abstract',
            'final',
            'normal',
        ],
    ],
    'php_unit_method_casing' => [
        'case' => 'camel_case',
    ],
];

$phpDocRules = [
    'align_multiline_comment' => [
        'comment_type' => 'all_multiline',
    ],
    'general_phpdoc_annotation_remove' => [
        'annotations' => [
            'author',
        ],
    ],
    'general_phpdoc_tag_rename' => [
        'case_sensitive' => false,
        'fix_annotation' => true,
        'fix_inline'     => true,
        'replacements'   => [
            'inheritDoc'  => 'inheritdoc',
            'inheritDocs' => 'inheritdoc',
        ],
    ],
    'no_blank_lines_after_phpdoc' => true,
    'no_empty_phpdoc'             => true,
    'no_superfluous_phpdoc_tags'  => [
        'allow_mixed'         => false,
        'allow_unused_params' => false,
        'remove_inheritdoc'   => false,
    ],
    'phpdoc_add_missing_param_annotation' => [
        'only_untyped' => false,
    ],
    'phpdoc_align' => [
        'align' => 'vertical',
        'tags'  => [
            'param',
            'property',
            'property-read',
            'property-write',
            'return',
            'throws',
            'type',
            'var',
            'method',
        ],
    ],
    'phpdoc_annotation_without_dot' => true,
    'phpdoc_indent'                 => true,
    'phpdoc_inline_tag_normalizer'  => [
        'tags' => [
            'example',
            'id',
            'internal',
            'inheritdoc',
            'inheritdocs',
            'link',
            'source',
            'toc',
            'tutorial',
        ],
    ],
    'phpdoc_line_span' => [
        'const'    => 'multi',
        'method'   => 'multi',
        'property' => 'multi',
    ],
    'phpdoc_no_access'    => true,
    'phpdoc_no_alias_tag' => [
        'replacements' => [
            'property-read'  => 'property',
            'property-write' => 'property',
            'type'           => 'var',
            'link'           => 'see',
        ],
    ],
    'phpdoc_no_empty_return'       => true,
    'phpdoc_no_package'            => true,
    'phpdoc_no_useless_inheritdoc' => true,
    'phpdoc_order'                 => [
        'order' => [
            'param',
            'return',
            'throws',
        ],
    ],
    'phpdoc_param_order'           => true,
    'phpdoc_return_self_reference' => [
        'replacements' => [
            'this'    => '$this',
            '@this'   => '$this',
            '$self'   => 'self',
            '@self'   => 'self',
            '$static' => 'static',
            '@static' => 'static',
        ],
    ],
    'phpdoc_scalar' => [
        'types' => [
            'boolean',
            'callback',
            'double',
            'integer',
            'real',
            'str',
        ],
    ],
    'phpdoc_separation'              => [
        'groups' => [
            ['author', 'copyright', 'license'],
            ['category', 'package', 'subpackage'],
            ['property', 'property-read', 'property-write'],
            ['deprecated', 'link', 'see', 'since'],
        ],
        'skip_unlisted_annotations' => false,
    ],
    'phpdoc_single_line_var_spacing' => true,
    'phpdoc_summary'                 => true,
    'phpdoc_tag_type'                => [
        'tags' => [
            'api'        => 'annotation',
            'author'     => 'annotation',
            'copyright'  => 'annotation',
            'deprecated' => 'annotation',
            'example'    => 'annotation',
            'global'     => 'annotation',
            'inheritDoc' => 'inline',
            'internal'   => 'annotation',
            'license'    => 'annotation',
            'method'     => 'annotation',
            'package'    => 'annotation',
            'param'      => 'annotation',
            'property'   => 'annotation',
            'return'     => 'annotation',
            'see'        => 'annotation',
            'since'      => 'annotation',
            'throws'     => 'annotation',
            'todo'       => 'annotation',
            'uses'       => 'annotation',
            'var'        => 'annotation',
            'version'    => 'annotation',
        ],
    ],
    'phpdoc_to_comment' => [
        'ignored_tags' => [],
    ],
    'phpdoc_trim'                                   => true,
    'phpdoc_trim_consecutive_blank_line_separation' => true,
    'phpdoc_types'                                  => [
        'groups' => [
            'simple',
            'alias',
            'meta',
        ],
    ],
    'phpdoc_types_order' => [
        'case_sensitive'  => false,
        'sort_algorithm'  => 'none',
        'null_adjustment' => 'always_first',
    ],
    'phpdoc_var_annotation_correct_order' => true,
    'phpdoc_var_without_name'             => true,
];

$returnNotationRules = [
    'no_useless_return'      => true,
    'return_assignment'      => true,
    'simplified_null_return' => true,
];

$semicolonRules = [
    'multiline_whitespace_before_semicolons' => [
        'strategy' => 'no_multi_line',
    ],
    'no_empty_statement'                         => true,
    'no_singleline_whitespace_before_semicolons' => true,
    'semicolon_after_instruction'                => true,
    'space_after_semicolon'                      => [
        'remove_in_empty_for_expressions' => true,
    ],
];

$strictRules = [
];

$stringNotationRules = [
    'explicit_string_variable'          => true,
    'heredoc_to_nowdoc'                 => true,
    'no_binary_string'                  => true,
    'simple_to_complex_string_variable' => true,
    'single_quote'                      => [
        'strings_containing_single_quote_chars' => true,
    ],
];

$whitespaceRules = [
    'array_indentation'           => true,
    'blank_line_before_statement' => [
        'statements' => [
            'break',
            'continue',
            'declare',
            'return',
            'throw',
            'try',
        ],
    ],
    'compact_nullable_type_declaration'   => true,
    'indentation_type'                    => true,
    'line_ending'                         => true,
    'method_chaining_indentation'         => true,
    'no_extra_blank_lines'                => [
        'tokens' => [
            'break',
            'case',
            'continue',
            'curly_brace_block',
            'default',
            'extra',
            'parenthesis_brace_block',
            'return',
            'square_brace_block',
            'switch',
            'throw',
            'use',
        ],
    ],
    'no_spaces_around_offset' => [
        'positions' => [
            'inside',
            'outside',
        ],
    ],
    'no_trailing_whitespace'       => true,
    'no_whitespace_in_blank_line'  => true,
    'single_blank_line_at_eof'     => true,
    'spaces_inside_parentheses'    => [
        'space' => 'none',
    ],
    'type_declaration_spaces' => [
        'elements' => ['function', 'property'],
    ],
    'types_spaces'                 => [
        'space' => 'none',
    ],
];

return array_merge(
    $aliasRules,
    $arrayRules,
    $attributeNotationRules,
    $basicRules,
    $casingRules,
    $castNotationRules,
    $classNotationRules,
    $classUsageRules,
    $commentRules,
    $constantNotationRules,
    $controlStructureRules,
    $doctrineAnnotationRules,
    $functionNotationRules,
    $importRules,
    $languageConstructRules,
    $listNotationRules,
    $namespaceNotationRules,
    $namingRules,
    $operatorRules,
    $phpTagRules,
    $phpUnitRules,
    $phpDocRules,
    $returnNotationRules,
    $semicolonRules,
    $strictRules,
    $stringNotationRules,
    $whitespaceRules,
);
