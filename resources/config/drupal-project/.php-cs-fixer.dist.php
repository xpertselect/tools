<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

include 'vendor/autoload.php';

$package = Composer\InstalledVersions::getRootPackage()['name'];
$rules   = include XpertSelect\Tools\ProjectType::DrupalProject->phpCsFixerRuleFile();

$rules['header_comment']['header'] = trim('
This file is part of the ' . $package . ' project.

This source file is subject to the license that is
bundled with this source code in the LICENSE.md file.
');

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__ . '/web/modules/custom',
        __DIR__ . '/web/profiles/custom',
        __DIR__ . '/web/themes/custom',
    ])
    ->name('*.inc')
    ->name('*.install')
    ->name('*.module')
    ->name('*.profile')
    ->name('*.php')
    ->name('*.theme')
    ->append([__FILE__])
    ->ignoreDotFiles(false)
    ->ignoreVCSIgnored(true);

return (new PhpCsFixer\Config('XpertSelect/Drupal'))
    ->setIndent('  ')
    ->setLineEnding("\n")
    ->setRules($rules)
    ->setFinder($finder);
