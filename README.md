# XpertSelect / Tools

[gitlab.com/xpertselect/tools](https://gitlab.com/xpertselect/tools)

XpertSelect package providing the tools and configurations for PHP development.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect/tools`](https://packagist.org/packages/xpertselect/tools) is done via [Composer](https://getcomposer.org).

```shell
composer require --dev composer-runtime-api ^2.2 xpertselect/tools
```

> This is purely a development dependency, do not include this package as a runtime dependency!

## Usage

Append the below instructions to `composer.json` to automatically create the latest configuration files when running
`composer update`:

```json
{
  "scripts": {
      "post-update-cmd": [
          "vendor/bin/xs-config --type={project-type} --force=false",
          "vendor/bin/xs-dependencies"
      ]
  }
}
```

The following `{project-type}`s are supported:

- `standard`
- `laravel-project`
- `laravel-package`
- `drupal-project`
- `drupal-package`

Set `--force=true` to always replace any existing configuration files. This will undo any local changes to these files!
